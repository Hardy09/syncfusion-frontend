export const environment = {
  production: true,

  urlPortalPrefix: 'portal',
  apiPrefix: 'http://168.119.24.22:4000/api/',
  socketUrl: 'ws://168.119.24.22:8082',
};
