import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { TreegridComponent } from './treegrid/treegrid.component';
// import the TreeGridModule for the TreeGrid component
import { TreeGridModule, PageService, SortService, FilterService, ToolbarService, EditService, ContextMenuService } from '@syncfusion/ej2-angular-treegrid';
import {HttpClientModule} from "@angular/common/http";


@NgModule({
  declarations: [
    AppComponent,
    TreegridComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    TreeGridModule,
    HttpClientModule,
  ],
  providers: [PageService, SortService, FilterService, ToolbarService, EditService, ContextMenuService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
