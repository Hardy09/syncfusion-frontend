import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {catchError, Subject, throwError} from "rxjs";

@Injectable({
  providedIn: 'root',
})
export class TreeGridService {

  private _allData$ = new Subject<any>();
  private _updatedData$ = new Subject<any>();
  private _deleteData$ = new Subject<any>();

  constructor(private httpClient: HttpClient) {
  }

  getData(url: string) {
    let mainUrl = environment.apiPrefix + url;
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.httpClient.get(mainUrl, {headers: httpHeaders})
      .subscribe((it: any) => {
          try {
            console.log(it);
            if (it.success) {
              this._allData$.next(it.result.message);
            } else {
              console.log(it.result.error);
            }
          } catch (e) {
            console.log(e);
          }
        }, catchError(err => {
          console.log(err);
          return throwError(err);
        })
      )
  }


  updateData(url: string, body: object) {
    let mainUrl = environment.apiPrefix + url;
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.httpClient.patch(mainUrl, body, {headers: httpHeaders})
      .subscribe((it: any) => {
          try {
            console.log(it);
            if (it.success) {
              this._updatedData$.next(it.result.message);
            } else {
              console.log(it.result.error);
            }
          } catch (e) {
            console.log(e);
          }
        }, catchError(err => {
          console.log(err);
          return throwError(err);
        })
      )
  }


  deleteData(url: string, body: object) {
    let mainUrl = environment.apiPrefix + url;
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    return this.httpClient.post(mainUrl, body, {headers: httpHeaders})
      .subscribe((it: any) => {
          try {
            console.log(it);
            if (it.success) {
              this._deleteData$.next(it.result.message);
            } else {
              console.log(it.result.error);
            }
          } catch (e) {
            console.log(e);
          }
        }, catchError(err => {
          console.log(err);
          return throwError(err);
        })
      )
  }

  get allData$() {
    return this._allData$.asObservable();
  }

  get updatedData$() {
    return this._updatedData$.asObservable();
  }

}
