import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {PageSettingsModel, EditSettingsModel, ToolbarItems} from '@syncfusion/ej2-angular-treegrid';
import {TreeGridService} from "./treeGridService";
import {webSocket} from "rxjs/webSocket";
import {environment} from "../../environments/environment";

@Component({
  selector: 'app-treegrid',
  template: `
    <ejs-treegrid [dataSource]='data' [treeColumnIndex]='1' [allowSorting]='true' [editSettings]='editSettings'
                  [toolbar]='toolbarOptions'
                  [contextMenuItems]='contextMenuItems' childMapping='Children'
                  (actionComplete)="actionHandler($event)" [selectedRowIndex]="selectedRowIndex"
    >
      <e-columns>
        <e-column field='EmployeeID' headerText='EmployeeID' [isPrimaryKey]='true' textAlign='Right' width=90>

        </e-column>
        <!--        <e-column field='Name' headerText='Name' textAlign='Right' width=80></e-column>-->
        <e-column field='FullName' headerText='FullName' textAlign='Left' width=90></e-column>
        <e-column field='Designation' headerText='Designation' textAlign='Right' width=90></e-column>
        <e-column field='Address' headerText='Address' textAlign='Right' width=80></e-column>
        <e-column field='Contact' headerText='Contact' textAlign='Left' width=90></e-column>
        <e-column field='Country' headerText='Country' textAlign='Right' width=90></e-column>
        <e-column field='DOB' headerText='DOB' textAlign='Right' width=80></e-column>
        <e-column field='DOJ' headerText='DOJ' textAlign='Right' width=90></e-column>
      </e-columns>
    </ejs-treegrid>
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TreegridComponent implements OnInit {

  public selectedRowIndex: any;
  public data: Object[] | undefined;
  public initialSort: Object | any;
  public pageSettings: PageSettingsModel | any;
  public editSettings: EditSettingsModel | any;
  public toolbarOptions: ToolbarItems[] | any;
  public contextMenuItems: Object[] | any;
  public editOptions: Object | any;
  public elem: HTMLElement | any;
  arr = ["FullName", "Designation", "Address", "Country", "Contact", "DOB", "DOJ"];
  subject = webSocket(`${environment.socketUrl}/all_data`);

  constructor(private treeGridService: TreeGridService, private cdr: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.treeGridService.allData$.subscribe((it) => {
      this.data = it;
      this.dataSocket();
      this.cdr.detectChanges();
    });
    this.treeGridService.updatedData$.subscribe((it) => {
      console.log("DATA UPDATED");
    });
    this.getData();
    this.initialSort = {
      columns: [{field: 'Name', direction: 'Ascending'}, {
        field: 'EmployeeID',
        direction: 'Ascending'
      }]
    };
    this.editSettings = {allowEditing: true, allowAdding: true, allowDeleting: true, mode: 'Dialog'};
    this.toolbarOptions = ['Edit', 'Delete', 'Update', 'Cancel'];
    this.contextMenuItems = ['AutoFit', 'AutoFitAll', 'SortAscending', 'SortDescending'];
  }

  actionHandler(event: any) {
    console.log("EDIT ", event); //custom Action
    if (event.requestType === "save") {
      let data = event.data;
      let child: any = [];
      let res: any = {};
      let parent = "";
      if (data['isParent']) {
        this.arr.forEach((it) => {
          child.push({
            id: data["EmployeeID"],
            key: it,
            value: data[it]
          })
        });
        res = {data: {parent: data["EmployeeID"], child}, isParent: data['isParent']};
        this.treeGridService.updateData('json/update_data', res);
      } else {
        this.arr.forEach((it) => {
          child.push({
            id: data["EmployeeID"],
            key: it,
            value: data[it]
          })
        });
        parent = data["parentItem"].EmployeeID;
        res = {data: [{parent, child}], isParent: data['isParent']};
        this.treeGridService.updateData('json/update_data', res);
      }
      console.log(res);

    }

    if (event.requestType === "delete") {
      let data = event.data;
      let isParent = false;
      let res: any = [];
      let res1 = "";
      for (let val of data) {
        if (val['isParent']) {
          res1 = val['EmployeeID'];
          isParent = val['isParent'];
          break;
        } else {
          let parent = val["parentItem"].EmployeeID;
          let obj = {
            parent,
            child: [val['EmployeeID']]
          };
          res.push(obj);
        }
      }
      console.log("BODY ", res);
      isParent ? this.treeGridService.deleteData('json/delete_data', {data: res1, isParent}) :
        this.treeGridService.deleteData('json/delete_data', {data: res, isParent})
    }
  }

  getData() {
    let url = 'json/get_data';
    this.treeGridService.getData(url);
  }

  dataSocket() {
    console.log("SOCKET =======");
    this.subject.subscribe((it: any) => {
      console.log("DATA FROM SOCKET ", it);
      this.data = it;
      this.cdr.detectChanges();
    });
  }


}
